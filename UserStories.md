# User Stories

Stories about how the intended users of the software currently work, used to guide the design of the software.

## Identity and Access Management

* A staff member or administrator logs in before performing any task on the system.
  * Roles available for accounts:
    * Staff
    * Administrator (also has all Staff privileges)

## A Guest Visiting Thea's Pantry

1. The staff member enters the guest's WSU ID number in the arrival screen.
    * This may be done with an ID card swipe or by entering into a field.
2. The system displays either
    * If the guest has never visited Thea's Pantry before
        1. A blank registration form is displayed<sup>1</sup>.
        2. The staff member requests information from the guest and fills out the form.
        3. The form data is stored in the database.
        4. The system displays a confirmation and reminds the staff member to tell the guest how much food they may take during their visit. (Currently in pounds, and only food. Toiletries and other items are not currently weighed.)
    * If the guest have visited Thea's Pantry previously
        1. The guest's current data is displayed.
        2. The staff member reviews the guest's information with them, and makes any corrections required.
        3. The system reminds the staff member to tell the guest how much food they may take during their visit.
            * If the user has visited previously in the current calendar week, the amount is reduced by the amount they took previously in the calendar week.
3. The system displays the checkout screen with this guest's WSU ID pre-filled.
4. The guest proceeds into the pantry to collect the items they want and returns to the front desk when done.
    * Currently only one guest is allowed in the pantry at a time.
5. The staff member enters the amount the guest is taking into the checkout screen.
6. The system records the amount (reducing the inventory amount), along with the WSU ID, and date/time.

<sup>1</sup> See [CustomerDocuments/TheasPantryLogEntry.pdf](./CustomerDocuments/TheasPantryLogEntry.pdf) for fields.

## New Inventory (Donation) Arrives at Thea's Pantry

1. The staff member enters the weight of new (food) inventory and source of the donation.
    * The source of the donation can be from a drop-down, with an "other" option to enter new donation sources (which will be added to the drop-down for future donations.)
        * Initial values for the drop-down:
            * Worcester County Food Bank
            * President's Holiday Party
            * Campus Food Drive
            * Private Donor
            * Anonymous Donor/Drop-off
2. The system records the amount (increasing the inventory amount), the source of the donation, and the date/time.

## A Pantry Administrator Looks Up the Current Inventory Level

1. The system verifies that the logged-in user is an Adminstrator.
2. The system displays the current inventory level.

## A Pantry Administrator Requests a Monthly Report for the Worcester County Food Bank

1. The system verifies that the logged-in user is an Adminstrator.
2. The user specifies the month and year for the report.
3. The system generates the report as a downloadable Excel file that the administrator can forward to the Worcester County Food Bank.<sup>2</sup>

<sup>2</sup> See [CustomerDocuments/November2020TheasPantryReport.pdf](./CustomerDocuments/November2020TheasPantryReport.pdf) for format.


---
Copyright &copy; 2021 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.