![Thea's Pantry](TheasPantry.png)

# Thea's Pantry

[Thea's Pantry](https://www.worcester.edu/Theas-Pantry/) &mdash; A food pantry for the Worcester State University community.

## This `Documentation` project contains details about the Thea's Pantry client solution, including design, development processes, and licensing.

**Note:** Thea's Pantry uses `main` for the default branch, rather than `master`.

* [User Stories](UserStories.md) &mdash; Stories about how the intended users of the software currently work, used to guide the design of the software.
* [Architecture](Architecture.md) &mdash; A high-level design for the pieces of the software system and how they interact with each other.
* [Technology](Technology.md) &mdash; A listing of the tools and frameworks we have decided to use to build and deploy the software system.
* [Workflow](Workflow.md) &mdash; The workflow to be used by developers.
* [Release Process](ReleaseProcess.md) &mdash; Release process for microservices.
* Licenses &mdash; We license all our code under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) and all other content under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

---
Copyright &copy; 2021 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.