# Architecture

Terminology is defined [here](https://gitlab.com/LibreFoodPantry/Community/-/issues/262#note_451997787) and [here](https://gitlab.com/LibreFoodPantry/Community/-/issues/262#note_472384025).

## Thea's Pantry Client Solution

The Thea's Pantry client solution uses a microservices architecture. 

## Systems

The client solution is composed of a number of systems:

* GuestInfoSystem
* InventorySystem
* ReportingSystem
* IAMSystem (Third-party)
* EventSystem (Third-party)

## Features

Each system is composed of features, which use components:

* GuestInfoSystem
  * CheckInGuest (feature) which uses:
    * CheckInGuestFrontend (component)
    * GuestInfoBackend (component)
  * GuestInfoBackend (component) which provides:
    * GetGuestInfo
    * UpdateGuestInfo
  * GuestInfoEvents (queue)
  * GuestInfoDb (component)
* InventorySystem
  * CheckOutGuest (feature) which uses:
    * CheckoutGuestFrontend (component)
    * InventoryBackend (component)
  * AddInventory (feature) which uses:
    * AddInventoryFrontend (component)
    * InventoryBackend (component)
  * CheckInventory (feature) which uses:
    * CheckInventoryFrontend (component)
    * InventoryBackend (component)
  * InventoryBackend (component) which provides:
    * AddInventory
    * ReduceInventory
    * GetInventory
  * InventoryEvents (queue)
  * InventoryDb (component)
* ReportingSystem
  * GenerateWCFBReport (feature) (Generate Worcester County Food Bank Report) which uses:
    * GenerateWCFBReportFrontend (component)
    * ReportingBackend (component)
  * ReportingBackend (component) which uses:
    * GuestInfoEvents (queue)
    * InventoryEvents (queue)
  * ReportingBackend
* IAMSystem (Third-party)
  * Login (feature)
  * ManageUsers (feature)
* EventSystem (Third-party)
  * SendEvent (feature)
  * SubscribeToEvents (feature)

## Integration/Deployment Diagrams

### GuestInfoSystem Standalone Integration Test

```plantuml
@startuml

skinparam ranksep 150

Actor Staff

node "StaffBrowser" {
  [Login] as StaffLogin
  [CheckInGuestFrontend] as BrowserCheckIn

}

package "GuestInfoSystem" {
  node NGINX <<Server>> {
    [CheckInGuestFrontend]
  }

  node RabbitMQ <<Server>> {
    queue GuestInfoEvents
  }

  node MongoDB <<Server>> {
    database GuestInfoDb
  }

  node Express.js <<Server>> {
    [GuestInfoBackend]
  }
  GuestInfoBackend --> GuestInfoEvents : backend-events-network
  GuestInfoBackend --> GuestInfoDb : backend-db-network

  node KeyCloak <<Server>> {
    [Login]
  }
}

Staff -> StaffBrowser
Login .up.> StaffLogin
StaffLogin --> Login
CheckInGuestFrontend ..> BrowserCheckIn
BrowserCheckIn --> GuestInfoBackend
StaffLogin <- BrowserCheckIn

@enduml
```

### InventorySystem Standalone Integration Test

```plantuml
@startuml

skinparam ranksep 150

Actor Staff
Actor Administrator

node "StaffBrowser" {
  [InventoryDirectoryFrontend] as StaffBrowserInventoryDirectory
  [AddInventoryFrontend] as BrowserAddInventory
  [Login] as StaffLogin
  [CheckOutGuestFrontend] as BrowserCheckOut
}

node "AdminBrowser" {
  [InventoryDirectoryFrontend] as AdminBrowserInventoryDirectory
  [Login] as AdminLogin
  [CheckInventoryFrontend] as BrowserCheckInventory
}

package "InventorySystem" {
  node NGINX <<Server>> as InventoryDirectoryNGINX {
    [InventoryDirectoryFrontend]
  }

  node NGINX <<Server>> as AddInventoryNGINX {
    [AddInventoryFrontend]
  }

  node NGINX <<Server>> as CheckOutGuestNGINX {
    [CheckOutGuestFrontend]
  }

  node NGINX <<Server>> as CheckInventoryNGINX {
    [CheckInventoryFrontend]
  }

  node RabbitMQ <<Server>> {
    queue InventoryEvents
  }

  node MongoDB <<Server>> {
    database InventoryDb
  }

  node Express.js <<Server>> {
    [InventoryBackend]
  }
  InventoryBackend --> InventoryEvents : backend-events-network
  InventoryBackend --> InventoryDb : backend-db-network

  node KeyCloak <<Server>> {
    [Login]
  }
}

Staff -down-> StaffBrowser
Administrator -down-> AdminBrowser
Login .up.> StaffLogin
StaffLogin --> Login
Login .up.> AdminLogin
AdminLogin --> Login
InventoryDirectoryFrontend .up.> StaffBrowserInventoryDirectory
InventoryDirectoryFrontend .up.> AdminBrowserInventoryDirectory
CheckOutGuestFrontend .up.> BrowserCheckOut
AddInventoryFrontend .up.> BrowserAddInventory
CheckInventoryFrontend .up.> BrowserCheckInventory
BrowserCheckOut --> InventoryBackend
BrowserAddInventory --> InventoryBackend
BrowserCheckInventory --> InventoryBackend
StaffLogin <- BrowserCheckOut
StaffLogin <- BrowserAddInventory
AdminLogin <- BrowserCheckInventory

@enduml
```

### ReportingSystem Standalone Integration Test

```plantuml
@startuml

skinparam ranksep 150

Actor Admin

node "AdminBrowser" {
  [Login] as AdminLogin
  [GenerateWCFBReportFrontend] as BrowserGenerateReport
}

package "ReportingSystem" {
  node NGINX <<Server>> {
    [GenerateWCFBReportFrontend]
  }

  node RabbitMQ <<Server>> {
    queue InventoryEvents
    queue GuestInfoEvents
  }

  node MongoDB <<Server>> {
    database ReportingDb
  }

  node Express.js <<Server>> {
    [ReportingBackend]
  }
  ReportingBackend --> InventoryEvents : backend-events-network
  ReportingBackend --> GuestInfoEvents : backend-events-network
  ReportingBackend --> ReportingDb : backend-db-network

  node KeyCloak <<Server>> {
    [Login]
  }
}

Admin -> AdminBrowser
Login .up.> AdminLogin
AdminLogin --> Login
GenerateWCFBReportFrontend .up.> BrowserGenerateReport
BrowserGenerateReport --> ReportingBackend
AdminLogin <- BrowserGenerateReport

@enduml
```

### Thea's Pantry Deployment Full Integration Test

```plantuml
@startuml

skinparam ranksep 150

Actor Staff
Actor Admin

node "StaffBrowser" {
  [DirectoryFrontend] as StaffBrowserDirectory
  [CheckInGuestFrontend] as BrowserCheckIn
  [CheckOutGuestFrontend] as BrowserCheckOut
  [AddInventoryFrontend] as BrowserAddInventory
  [Login] as StaffLogin
  StaffLogin <- BrowserCheckOut
  StaffLogin <- BrowserAddInventory
  StaffLogin <- BrowserCheckIn
}

node "AdminBrowser" {
  [DirectoryFrontend] as AdminBrowserDirectory
  [CheckInventoryFrontend] as BrowserCheckInventory
  [GenerateWCFBReportFrontend] as BrowserGenerateReport
  [Login] as AdminLogin
  [ManageUsers] as AdminManageUsers
  AdminLogin <- BrowserCheckInventory
  AdminLogin <- BrowserGenerateReport
}

package DirectorySystem {
  node NGINX <<Server>> as DirectoryNGINX {
  [DirectoryFrontend]
  }
}

package "IAMSystem" {
  node KeyCloak <<Server>> {
      [Login] as Login
      [ManageUsers] as ManageUsers
  }
}

package "GuestInfoSystem" {
  node NGINX <<Server>> as GuestInfoNGINX {
    [CheckInGuestFrontend]
  }

  node MongoDB <<Server>> as GuestInfoMongoDB{
    database GuestInfoDb
  }

  node Express.js <<Server>> as GuestInfoExpress.js{
    [GuestInfoBackend]
  }
  GuestInfoBackend --> GuestInfoDb : guestinfo-backend-db-network
}

package "InventorySystem" {
  node NGINX <<Server>> as AddInventoryNGINX {
    [AddInventoryFrontend]
  }

  node NGINX <<Server>> as CheckOutGuestNGINX {
    [CheckOutGuestFrontend]
  }
  
  node NGINX <<Server>> as CheckInventoryNGINX {
    [CheckInventoryFrontend]
  }

  node MongoDB <<Server>> as InventoryMongoDB {
    database InventoryDb
  }

  node Express.js <<Server>> as InventoryExpress.js{
    [InventoryBackend]
  }
  InventoryBackend --> InventoryDb : inventory-backend-db-network
}

package "ReportingSystem" {
  node NGINX <<Server>> as ReportingNGINX {
    [GenerateWCFBReportFrontend]
  }

  node MongoDB <<Server>> as ReportingMongoDB {
    database ReportingDb
  }

  node Express.js <<Server>> as ReportingExpress.js {
    [ReportingBackend]
  }
  ReportingBackend --> ReportingDb : reporting-backend-db-network
}

package "EventSystem" {
  node RabbitMQ <<Server>> {
    queue InventoryEvents
    queue GuestInfoEvents
  }
}

Staff --> StaffBrowser
Admin --> AdminBrowser

Login .down.> StaffLogin
StaffLogin -up-> Login
Login .down.> AdminLogin
AdminLogin -up-> Login
ManageUsers .up.> AdminManageUsers
AdminManageUsers --> ManageUsers

DirectoryFrontend .down.> StaffBrowserDirectory
DirectoryFrontend .down.> AdminBrowserDirectory
CheckInGuestFrontend ..> BrowserCheckIn
CheckOutGuestFrontend .up.> BrowserCheckOut
AddInventoryFrontend .up.> BrowserAddInventory
CheckInventoryFrontend .up.> BrowserCheckInventory
GenerateWCFBReportFrontend .up.> BrowserGenerateReport
BrowserCheckIn --> GuestInfoBackend
BrowserCheckOut --> InventoryBackend
BrowserAddInventory --> InventoryBackend
BrowserCheckInventory --> InventoryBackend
BrowserGenerateReport --> ReportingBackend
ReportingBackend ----> InventoryEvents : events-network
ReportingBackend ----> GuestInfoEvents : events-network
InventoryBackend ----> InventoryEvents : events-network
GuestInfoBackend ----> GuestInfoEvents : events-network

@enduml
```

---
Copyright &copy; 2021 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.