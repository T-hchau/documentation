# Thea's Pantry Workflow

1. Developers work in branches - a new branch for each feature/fix/refactor - each associated with a merge request when the branch is created.
2. Merge requests will be set to squash commits when the merge is approved.
3. Developers are expected to use [`Conventional Commits`](https://www.conventionalcommits.org/en/v1.0.0/) and push each commit to their branch after committing.
4. [`commitlint`](https://github.com/conventional-changelog/commitlint) will run automatically on each individual push/commit. However, a `commitlint` failure on a commit message will not cause the push to fail, as these individual commit messages will not become part of the final changelog or be used to bump the version number. The results of the `commitlint` messages will be used to help developers learn to write correct commit messages, for the (squashed) merge commits that *will* be used for the changelog and version number.
5. When approving a merge commit, it the responsibility of the approver to review the work to be merged and to write a commit message that 
    - uses a Conventional Commit `type` to correctly increment the version number.
    - has a `description` that accurately and succinctly describes the work
    - contains `Co-authored-by:` trailers for all of the authors in the squashed commits (this may be automated in the future)
    - `closes` the associated issue in the issue tracker
    - uses a `BREAKING CHANGE` footer *or* `!` after the `type` if the work introduces a breaking API change

    ---
Copyright &copy; 2021 The LibreFoodPantry Authors. This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
